import 'package:grinder/grinder.dart';
import 'package:grind_publish/grind_publish.dart' as grind_publish;

@Task('Automatically publishes this package if the pubspec version increases')
void autoPublish() async {
  // Will use the `ACCESS_TOKEN`, `REFRESH_TOKEN` and `EXPIRATION` env vars
  // as a configuration.
  final credentials = grind_publish.Credentials.fromEnvironment();

  await grind_publish.autoPublish('your-package-name', credentials);
}
