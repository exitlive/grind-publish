import 'dart:async';
import 'dart:io';

import 'package:logging/logging.dart';
import 'package:grinder/grinder.dart';
import 'package:grind_publish/grind_publish.dart' as grind_publish;

@Task('Runs all tests required to pass')
@Depends(analyze, unitTest, checkFormat)
bool test() => true;

@Task('Runs the unit tests for this library')
Future unitTest() =>
    // Setting concurrency to 1 because it makes the output more readable and
    // the project is quite small anyway
    TestRunner().testAsync(concurrency: 1);

@Task('Runs dartanalyzer and makes sure there are no warnings or lint proplems')
Future<Null> analyze() async {
  await runAsync('dartanalyzer',
      arguments: ['.', '--fatal-hints', '--fatal-warnings', '--fatal-lints']);
}

@Task()
void checkFormat() {
  if (DartFmt.dryRun(Directory('.'))) {
    fail('Code is not properly formatted. Run `grind format`');
  }
}

@Task()
void format() => DartFmt.format(Directory('.'));

@Task('Automatically publishes this package if the pubspec version increases')
Future<void> autoPublish() async {
  final credentials = grind_publish.Credentials.fromEnvironment();
  await grind_publish.autoPublish('grind_publish', credentials);
}

void main(List<String> args) {
  Logger.root
    ..onRecord.listen((record) => log(record.message))
    ..level = Level.ALL;

  grind(args);
}
