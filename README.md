# grind_publish

This package automatically publishes your library when the `pubspec.yaml`
version has been increased and merged into master.

> **NOTE**: We strongly recommend to add a comment to the `pubspec.yaml`
> file to note that this package auto publishes if the version is
> increased. E.g.:
>
>     # Increasing this version will automatically
>     # publish to pub when merged into master
>     version: 0.0.2

## Usage

### 1. Setup the grind task like this:

```dart
@Task('Automatically publishes this package if the pubspec version increases')
autoPublish() async {
  final credentials = grind_publish.Credentials.fromEnvironment();
  await grind_publish.autoPublish('your-package-name', credentials);
}
```

### 2. Set the environmental variables in your CI runner

By default the env vars are named: `ACCESS_TOKEN`, `REFRESH_TOKEN`, `EXPIRATION`,
`TOKEN_ENDPOINT` (*optional*), `SCOPES` (*optional*).

You find the values on your local machine in `~/.pub-cache/credentials.json`
after you once ran `pub publish`.

On GitLab, setting these variables is in **Settings → CI / CD → Variables**.

### 3. Run the script in your CI pipeline

This really depends on where you are running this. For GitLab example, simply look
at this [`.gitlab-ci.yml`](.gitlab-ci.yml). 

# License

MIT