import 'package:grind_publish/grind_publish.dart';
import 'package:test/test.dart';

void main() {
  group('Credentials', () {
    final expiration = 1533742204172;
    test('properly creates json from default constructor', () {
      final credentials = Credentials(
          accessToken: 'acctok',
          refreshToken: 'refreshtok',
          expiration: expiration,
          tokenEndpoint: 'https://endpoint',
          scopes: ['https:/scope']);

      final json = credentials.toJson();
      expect(
          json,
          '{"accessToken":"acctok",'
          '"refreshToken":"refreshtok",'
          '"expiration":$expiration,'
          '"tokenEndpoint":"https://endpoint",'
          '"scopes":["https:/scope"]}');
    });
    test('properly creates json from env vars', () {
      testEnvironment = <String, String>{
        'ACCESS_TOKEN': 'acctok',
        'REFRESH_TOKEN': 'refreshtok',
        'EXPIRATION': expiration.toString(),
        'TOKEN_ENDPOINT': 'https://endpoint',
        'SCOPES': '["https:/scope"]',
      };
      final credentials = Credentials.fromEnvironment();

      final json = credentials.toJson();
      expect(
          json,
          '{"accessToken":"acctok",'
          '"refreshToken":"refreshtok",'
          '"expiration":$expiration,'
          '"tokenEndpoint":"https://endpoint",'
          '"scopes":["https:/scope"]}');
    });
    test('handles optional null values properly', () {
      testEnvironment = <String, String>{
        'ACCESS_TOKEN': 'acctok',
        'REFRESH_TOKEN': 'refreshtok',
        'EXPIRATION': expiration.toString(),
      };
      final credentials = Credentials.fromEnvironment();

      final json = credentials.toJson();
      expect(
          json,
          '{"accessToken":"acctok",'
          '"refreshToken":"refreshtok",'
          '"expiration":$expiration,'
          '"tokenEndpoint":"https://accounts.google.com/o/oauth2/token",'
          '"scopes":["openid","https://www.googleapis.com/auth/userinfo.email"]}');
    });
  });
}
